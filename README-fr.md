# EasyLAN

Globalement, cette solution vise à déployer une application conteneurisée rapidement sans avoir à gérer différents aspects récurent, voir rébarbatif, comme la mise en place d'un proxy HTTP, un certificat SSL ou encore un serveur DNS.

Liste des services fournis :
* Génération automatisée de certificat SSL avec une PKI et un CA auto-signé et local ou à partir de Let's Encrypt
* Génération automatisée d'un proxy HTTPS pour l'ensemble des services
* Génération automatisée d'un serveur DNS avec une zone DNS pour les services locaux

Cette solution est conçue principalement pour les cas suivants :
* Déploiement d'application web dans un réseau privé pour une des raisons comme :
  * vous n'avez pas la possibilité de vous procurer un certificat valide fourni par un CA reconnu
  * vous n'avez pas la possibilité de vous procurer un nom de domaine public
* Pour un environnement de test ou développement, pour tester une application sans avoir à gérer la partie proxy/SSL/DNS

## Sommaire
* [Présentation générale du projet EasyLAN](#présentation-générale-du-projet-easylan)
* [Liste des services principaux](#liste-des-services-principaux)
  * [Service EasyLAN](#service-easylan)
    * [Configuration du service](#configuration-du-service)
    * [Paramétrage des certificats pour chacun des services](#paramétrage-des-certificats-pour-chacun-des-services)
  * [Service DNS](#service-dns)
    * [Description du service](#description-du-service)
    * [Paramétrage de la fonctionnalité DNS des services](#paramétrage-de-la-fonctionnalité-dns-des-services)
  * [Service Proxy HTTPS](#service-proxy-https)
    * [Description du service](#description-du-service)
    * [Paramétrage de la fonctionnalité DNS des services](#paramétrage-de-la-fonctionnalité-dns-des-services)
  * [Format des labels de type `lan.easylan.****.config`](#format-des-labels-de-type-laneasylanconfig)

## Présentation générale du projet EasyLAN

![Organisation du projet](./doc/img/project_architecture.png)

Le projet comporte d'une part les services essentiels ("core services" sur le schéma) et d'autres part les services/applications utilisateur ("guest services" sur le schéma).

Arborescence du projet est composé entre autres :
* du fichier `docker-compose.yml` qui contient la description des services essentiels
* du dossier `docker-compose.d` qui contient la description des services utilisateur dans un fichier `nom_du_service.yml` surchargeable par le fichier `nom_du_service.override.yml`
* du dossier `env.d` qui contient les définitions des environnements des services, où `nom_du_service.default.env` servant de référence au fichier `nom_du_service.env`
* du fichier `config.default.env` servant de référence au fichier `config.env` qui contient les paramètres globaux
* du fichier `easylan.sh` qui surcharge la commande `docker-compose` en utilisant les fichiers `docker-compose.yml` ainsi que les fichiers `yml` du dossier `docker-compose.d` en fonction des services activés dans le fichier `config.env`
* du dossier data (créé au premier démarrage) contenant entre autre les certificats généré dont le fichier `data/ssl/easylan-ca.crt` qui contient le certificat racine

## Liste des services principaux

Un script à la racine `easylan.sh` surcharge la commande `docker-compose` et permet, en se basant sur la configuration `config.env` de lancer les différents services activés.

Guide de démarrage rapide :
* Exécutez la commande `./easylan.sh config` pour initialiser la configuration
* Éditez la configuration `config.env` en mettant à jour les variables :
  * EASYLAN_APPLICATION_LIST : liste des applications à activer (ex: EASYLAN_APPLICATION_LIST=(application1 application2 application3) )
  * EASYLAN_DNS_LISTEN : adresse et port d'écoute du service DNS. Peut-être réglé sur 0.0.0.0:53 (ou 192.168.1.10:53 si on se réfère au schéma)
  * EASYLAN_PROJECT_NAME (paramètre avancé) : nom du projet docker-compose (utilisation avancée)
  * EASYLAN_NET4_BASE  (paramètre avancé): Base de l'adresse réseau du sous-réseau des services (utilisation avancée)
  * EASYLAN_DNS_SERVER (paramètre avancé) : IP interne du serveur DNS (utilisation avancée)
* Exécutez de nouveau la commande `./easylan.sh config` pour initialiser la configuration des applications que vous avez activés à l'étape précédente
* Passez à la configuration des services principaux (fichier `env.d/easylan.env`, voir rubriques ci-dessous) ainsi que des applications avant de passer à l'étape suivante
* En fin, exécutez la commande `./easylan.sh start`

### Service EasyLAN

#### Configuration du service

Liste des variables d'environnement (fichier env.d/easylan.env) :
* EASYLAN_TLD : Extension de domaine à utiliser, duquel découleront les sous domaines du type mon-service.mon-extension (exemple `lan` pour une utilisation en local, ou exemple.com, ou encore, mes-services-lan.exemple.com)
* EASYLAN_CA_CN : Nom de l'autorité de certification locale
* EASYLAN_EXTERNAL_IP4 : IP (v4) externe du serveur, soit par example 192.168.1.10 si on se réfère au schéma (il ne s'agit pas forcément de l'adresse IP publique)
* EASYLAN_EXTERNAL_IP6 : IP (v6) externe du serveur (même remarques que pour le paramètre EASYLAN_EXTERNAL_IP4)
* EASYLAN_CERT_TYPE : Type de certificat : `easyrsa` (par défaut) pour une utilisation en local avec une autorité de certification auto-signée, ou bien `letsencrypt` pour générer des certificats en utilisant le service Let's Encrypt (nécessite un domaine public avec un champ DNS CNAME de type wildcard vers le serveur)
* EASYLAN_KEYGEN_OPTS (paramètre avancé) : Option de génération de clé pour la commande easyrsa. Il est recommandé de laisser l'algorithme à RSA pour éviter les problèmes d'incompatibilité liés à certains services
* EASYLAN_CERTBOT_TEST (utilisation avec EASYLAN_CERT_TYPE=letsencrypt) : Génération de certificat de test
* EASYLAN_CERTBOT_EMAIL (utilisation avec EASYLAN_CERT_TYPE=letsencrypt) : Courrier électronique valide requis pour l'enregistrement auprès de Let's Encrypt

#### Paramétrage des certificats pour chacun des services

Liste des possibilités dans l'ordre basique à avancé :
* Pour activer cette fonctionnalité il faut ajouter le label suivant : `lan.easylan.ssl.enable: true`
* Pour définir une configuration personnalisée (qui remplace la configuration par défaut) il faut ajouter le label suivant : `lan.easylan.ssl.config: "ma config"` en utilisant le [format suivant](#format-des-labels-de-type-laneasylanconfig)
* La configuration par défaut est un élément de configuration avec toutes les valeurs (liste ci-dessous) par défaut
* Liste des champs de la configuration :
  * `cn` (par défaut : `nom_du_service.$EASYLAN_TLD`) : Nom de domaine principal du service
  * `dns-alt-names` : Noms de domaine alternatifs du service
  * `key` (par défaut : `nom_du_service.$EASYLAN_TLD.key`) : nom du fichier de clé
  * `crt` (par défaut : `nom_du_service.$EASYLAN_TLD.crt`) : Nom du fichier de certificat
  * `ca` (par défaut : `ca.crt`) : Nom du fichier de l'autorité de certification
  * `dhparam` (par défaut : aucun) : Fichier Diffie-Hellman
  * `dhparam-size` (par défaut : `1024`) : Taille de la clé Diffie-Hellman
  * `key-uid`, `crt-uid`, `ca-uid`, `dhparam-uid` (par défaut : `0`) : Identifiant numérique de l'utilisateur associé au fichier
  * `key-gid`, `crt-gid`, `ca-gid`, `dhparam-gid` (par défaut : `0`) : Identifiant numérique du groupe associé au fichier
  * `key-mod`, `crt-mod`, `ca-mod`, `dhparam-mod` (par défaut : `0700`) : Permissions au format numérique associé au fichier

Exemples : le service proxy HTTP (fichier [docker-compose.yml](./docker-compose.yml)) avec un certificat contenant le nom `proxy.$EASYLAN_TLD` ainsi que les non des applications stocké dans la variable $EASYLAN_PROXY_SERVERS

### Service DNS

#### Description du service
Le service DNS contient la configuration de la zone `$EASYLAN_TLD` avec :
* une vue interne pour les services et application du réseau "backend" qui effectue la résolution du nom vers l'IP interne s’il est interrogé depuis ce réseau interne
* une vue externe renverra systématiquement `$EASYLAN_EXTERNAL_IP4` ou `$EASYLAN_EXTERNAL_IP6` (192.168.1.10 sur le schéma) pour les services accessibles de l'extérieur (ex: le proxy HTTPS, le serveur DNS ou encore, le serveur MySQL sur l'exemple)

#### Paramétrage de la fonctionnalité DNS des services

Liste des possibilités dans l'ordre basique à avancé :
* Pour activer cette fonctionnalité il faut ajouter le label suivant : `lan.easylan.dns.enable: true`
* Pour définir une configuration personnalisée (qui remplace la configuration par défaut) il faut ajouter le label suivant : `lan.easylan.dns.config: "ma config"` en utilisant le [format suivant](#format-des-labels-de-type-laneasylanconfig)
* La configuration par défaut est un élément de configuration avec toutes les valeurs (liste ci-dessous) par défaut
* Liste des champs de la configuration :
  * `name` (par défaut : nom du service ou nom d'hôte s’il est spécifié) : nom qui donnera le DNS nom.nom_de_la_zone (exemple `mysql.lan` ou `mysql.example.com`)
  * `ip6-int` (par défaut : IPv6 du docker sur le réseau privé) : Définition de l'IPv6 sur la vue interne
  * `ip4-int` (par défaut : IPv4 du docker sur le réseau privé) : Définition de l'IPv4 sur la vue interne
  * `ip6-ext` (par défaut : valeur de la variable d'environnement EASYLAN_EXTERNAL_IP6) : Définition de l'IPv6 sur la vue externe
  * `ip4-ext` (par défaut : valeur de la variable d'environnement EASYLAN_EXTERNAL_IP4) : Définition de l'IPv4 sur la vue externe

Exemples :
* Application PostgreSQL (fichier [docker-compose.d/postgres.yml](./docker-compose.d/postgres.yml)) : Activation simple avec le nom généré par défaut (`postgres.$EASYLAN_TLD`)
* Application MariaDB (fichier [docker-compose.d/mariadb.yml](./docker-compose.d/mariadb.yml)) : Activation + définition de deux nom (`mariadb.$EASYLAN_TLD` et `mysql.$EASYLAN_TLD`)

### Service Proxy HTTPS

#### Description du service

Le service Proxy HTTPS est un service utilisable par l'ensemble des applications qui permet :
* Définition rapide et facile d'un proxy HTTP
* Communication sécurisée via HTTPS inclue

#### Paramétrage de la fonctionnalité Proxy des services

Liste des possibilités dans l'ordre basique à avancé :
* Pour activer cette fonctionnalité il faut ajouter le label suivant : `lan.easylan.proxy.enable: true`
* Pour définir une configuration personnalisée (qui remplace la configuration par défaut) il faut ajouter le label suivant : `lan.easylan.proxy.config: "ma config"` en utilisant le [format suivant](#format-des-labels-de-type-laneasylanconfig)
* La configuration par défaut est un élément de configuration avec toutes les valeurs (liste ci-dessous) par défaut
* Liste des champs de la configuration :
  * `server` (par défaut : nom du service ou nom d'hôte s’il est spécifié) : Nom du serveur (nom de domaine) permettant d'accéder à l'application via le proxy
  * `proto` (par défaut : `http`) : protocole de l'application cible sur lequel le proxy va rediriger le flux (http ou https)
  * `address` (par défaut : `$ip_addres`) : adresse de l'application cible sur lequel le proxy va rediriger le flux
  * `port` (par défaut : `80`) : port de l'application cible sur lequel le proxy va rediriger le flux
  * `template` (par défaut : `generic`) : modèle de fichier à utiliser pour la définition de la configuration de proxy

Exemples :
* Application Nextcloud (fichier [docker-compose.d/nextcloud.yml](./docker-compose.d/nextcloud.yml)) : Activation simple avec le nom généré par défaut, ici à partir du hostname suffixé par le TLD ou nom de domaine choisi lors de la configuration (`cloud.$EASYLAN_TLD`), donc si on se réfère au schéma d'exemple, Toute requêtes adressées sur https://cloud.lan sera redirigée sur http://172.22.0.7:80
* Application MariaDB (fichier [docker-compose.d/miniflux.yml](./docker-compose.d/miniflux.yml)) : Activation + définition d'une configuration proxy vers un port personnalisé 8080

### Format des labels de type `lan.easylan.****.config`
Chaque champ `config` est un champ texte qui contient une ou plusieurs configurations séparées par un espace. Chacune de ces configuration est une suite de `clé=valeur` séparés par une virgule.
Exemple de configuration `clé1=valeur1 clé2=valeur2,clé3=valeur3` : Il y a deux configurations : `clé1=valeur1` et `clé2=valeur2,clé3=valeur3`
