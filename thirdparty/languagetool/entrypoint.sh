#!/bin/bash -e

# display a message
print_log() {
    local level=$1;
    local fmt="%s"
    shift || print_log C "Usage print_log <level> [-f <fmt>] message"
    if [ "$1" == "-f" ]; then
        fmt=$2
        shift 2 || true
    fi
    case "${level,,}" in
        c|critical)
            >&2 printf "\033[91m[CRIT] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            exit 1
            ;;
        e|error)
            >&2 printf "\033[91m[ERRO] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        w|warning)
            >&2 printf "\033[93m[WARN] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        n|note)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$@"
            ;;
        i|info)
            printf "\033[92m[INFO] $fmt\033[0m\n" "$@"
            ;;
        *)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$level" "$@"
            ;;
    esac
}

: ${LANGUAGETOOL_NGRAMS_DIR:="/opt/LanguageTool/ngrams"}
: ${LANGUAGETOOL_LISTEN_PORT:="8080"}

CMD_LINE=(java -cp languagetool-server.jar org.languagetool.server.HTTPServer --public --allow-origin '*' --port "$LANGUAGETOOL_LISTEN_PORT")

if [ -d "$LANGUAGETOOL_NGRAMS_DIR" ]; then
    CMD_LINE+=(--languageModel "$LANGUAGETOOL_NGRAMS_DIR")
    eval NGRAM_FILES=($(
        curl -Ls https://languagetool.org/download/ngram-data/untested/ | \
            sed -E 's/^.*(ngram-[a-z]{2}-[0-9]{8}.zip).*|.*$/\1/; /^$/d; s_^(.*)$_untested/\1_g'
        curl -Ls https://languagetool.org/download/ngram-data/ | \
            sed -E 's/^.*(ngrams-[a-z]{2}-[0-9]{8}.zip).*|.*$/\1/; /^$/d'
    ))

    declare -A LANGUAGETOOL_AVAILABLE_NGRAMS_FILES
    print_log N "Available ngram files:"
    for file in "${NGRAM_FILES[@]}"; do
        lang=$(sed -E 's/.*-([a-z]{2})-[0-9]{8}.zip/\1/g' <<< "$file")
        LANGUAGETOOL_AVAILABLE_NGRAMS_FILES[$lang]=$file
        print_log N " * [$lang]: $file"
    done

    for lang in $LANGUAGETOOL_NGRAMS_LIST; do
        if [ -d "$LANGUAGETOOL_NGRAMS_DIR/$lang" ] && [ "$LANGUAGETOOL_NGRAMS_AUTO_UPDATE" != "true" ]; then
            continue
        fi
        file=${LANGUAGETOOL_AVAILABLE_NGRAMS_FILES[$lang]}
        if [ -f "$LANGUAGETOOL_NGRAMS_DIR/$lang/.source" ] && [ "$(cat $LANGUAGETOOL_NGRAMS_DIR/$lang/.source)" == "$file" ]; then
            continue
        fi
        rm -rf "$LANGUAGETOOL_NGRAMS_DIR/.dl"
        mkdir -p "$LANGUAGETOOL_NGRAMS_DIR/.dl"
        print_log N "Downloading $lang language, file $file..."
        wget https://www.languagetool.org/download/ngram-data/$file \
            --progress bar:force:noscroll --output-document "$LANGUAGETOOL_NGRAMS_DIR/.dl/ngrams.zip"
        print_log N "Extracting $lang language, file $file..."
        unzip -q -d "$LANGUAGETOOL_NGRAMS_DIR/.dl" "$LANGUAGETOOL_NGRAMS_DIR/.dl/ngrams.zip"
        rm "$LANGUAGETOOL_NGRAMS_DIR/.dl/ngrams.zip"
        echo "$file" "$LANGUAGETOOL_NGRAMS_DIR/.dl/$lang/.source"
        print_log N "Installing $lang language, file $file..."
        rm -rf "$LANGUAGETOOL_NGRAMS_DIR/$lang"
        mv "$LANGUAGETOOL_NGRAMS_DIR/.dl/$lang" "$LANGUAGETOOL_NGRAMS_DIR/$lang"
        print_log I "Language $lang (file $file) successfully installed."
    done
    rm -rf "$LANGUAGETOOL_NGRAMS_DIR/.dl"
fi

exec "${CMD_LINE[@]}"
