#!/bin/bash -e

: ${PRIVACYIDEA_LISTEN_ADDRESS:=127.0.0.1}
: ${PRIVACYIDEA_LISTEN_PORT:=5000}
: ${PRIVACYIDEA_ADMIN_LOGIN:=admin}
: ${PRIVACYIDEA_CONFIGFILE:="/etc/privacyidea/pi.cfg"}
: ${PRIVACYIDEA_ADMIN_PASSWORD:="admin"}
: ${PRIVACYIDEA_ADMIN_EMAIL:="admin@lan"}

PRIVACYIDEA_VARLIST=(
    SQLALCHEMY_DATABASE_URI
    SECRET_KEY
    PI_PEPPER
    PI_ENCFILE
    PI_AUDIT_KEY_PRIVATE
    PI_AUDIT_KEY_PUBLIC
)

if [ ! -f "$PRIVACYIDEA_CONFIGFILE" ]; then
    for var in "${PRIVACYIDEA_VARLIST[@]}"; do
        if ! [[ -v $var ]]; then
            continue
        fi
        echo "$var='${!var}'" >> "$PRIVACYIDEA_CONFIGFILE"
    done
    pi-manage createdb
    pi-manage admin add -e "$PRIVACYIDEA_ADMIN_EMAIL" -p "$PRIVACYIDEA_ADMIN_PASSWORD" "$PRIVACYIDEA_ADMIN_LOGIN"
    pi-manage create_pgp_keys
    pi-manage create_enckey
    pi-manage create_audit_keys
fi

pi-manage runserver --host "$PRIVACYIDEA_LISTEN_ADDRESS" --port "$PRIVACYIDEA_LISTEN_PORT" "$@"
