#!/bin/bash -e

> /tmp/node_extra_ca_certs.crt
cat /usr/local/share/ca-certificates/* > /tmp/node_extra_ca_certs.crt 2>/dev/null || true

sed --regexp-extended -i \
    's|^environment=(.*)$|environment=NODE_EXTRA_CA_CERTS=/tmp/node_extra_ca_certs.crt,\1|' \
    /etc/onlyoffice/documentserver/supervisor/*.conf

update-ca-certificates

/app/onlyoffice/run-document-server.sh
