#!/bin/bash -e

# display a message
print_log() {
    local level=$1;
    local fmt="%s"
    shift || print_log C "Usage print_log <level> [-f <fmt>] message"
    if [ "$1" == "-f" ]; then
        fmt=$2
        shift 2 || true
    fi
    case "${level,,}" in
        c|critical)
            >&2 printf "\033[91m[CRIT] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            exit 1
            ;;
        e|error)
            >&2 printf "\033[91m[ERRO] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        w|warning)
            >&2 printf "\033[93m[WARN] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        n|note)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$@"
            ;;
        i|info)
            printf "\033[92m[INFO] $fmt\033[0m\n" "$@"
            ;;
        *)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$level" "$@"
            ;;
    esac
}


WDIR="$(readlink -f "$(dirname "$0")")"

DOCKER_COMPOSE_COMMAND=($(which docker-compose 2>/dev/null || true))
if [ ${#DOCKER_COMPOSE_COMMAND[@]} -ne 1 ]; then
    mkdir -p "$WDIR/data/bin"
    if [ ! -x "$WDIR/data/bin/docker-compose" ]; then
        get_last_git_tag() {
            git ls-remote "$1" | gawk -e '
                BEGIN{
                    f = 0
                }
                match($2, /^refs\/tags\/(v)?([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)$/, ret) {
                    v=sprintf("%02d%02d%02d", ret[2], ret[3], ret[4]);
                    if (f < v) {
                        f = v;
                    }
                }
                END{
                    printf("%d.%d.%d\n", substr(f, 1, 2), substr(f, 3, 2) , substr(f, 5, 2))
                }'
        }
        curl -L "https://github.com/docker/compose/releases/download/$(
            get_last_git_tag https://github.com/docker/compose)/docker-compose-$(uname -s)-$(uname -m)" \
            -o "$WDIR/data/bin/docker-compose"
        chmod +x "$WDIR/data/bin/docker-compose"
    fi
    DOCKER_COMPOSE_COMMAND=("$WDIR/data/bin/docker-compose")
fi

ENV_D="$WDIR/env.d"
DOCKER_COMPOSE_D="$WDIR/docker-compose.d"

init_config_file() {
    local cfg=$1
    local cfg_def="${cfg%.env}.default.env"
    if [ ! -f "$cfg" ] && [ -f "$cfg_def" ]; then
        print_log W "$cfg file not found, initializing it using $cfg_def"
        cp "$cfg_def" "$cfg"
    fi
}

init_config_file "$WDIR/config.env"
. "$WDIR/config.env"
init_config_file "$ENV_D/easylan.env"

: ${EASYLAN_PROJECT_NAME:="easylan"}
DOCKER_COMPOSE_COMMAND+=(-p "$EASYLAN_PROJECT_NAME" -f "$WDIR/docker-compose.yml")

for app in "${EASYLAN_APPLICATION_LIST[@]}"; do
    if [ ! -f "$DOCKER_COMPOSE_D/$app.yml" ]; then
        print_log C "Module '$app' not found!"
    fi
    init_config_file "$ENV_D/$app.env"
    DOCKER_COMPOSE_COMMAND+=(-f "$DOCKER_COMPOSE_D/$app.yml")
    if [ -f "$DOCKER_COMPOSE_D/$app.override.yml" ]; then
        DOCKER_COMPOSE_COMMAND+=(-f "$DOCKER_COMPOSE_D/$app.override.yml")
    fi
done
if [ -f "$WDIR/docker-compose.override.yml" ]; then
    DOCKER_COMPOSE_COMMAND+=(-f "$WDIR/docker-compose.override.yml")
fi

_docker_compose() {
    "${DOCKER_COMPOSE_COMMAND[@]}" --project-directory "$APPS_DIR" "$@"
}

for c in "$@"; do
    if [ "${c:0:1}" != "-" ]; then
        COMMAND=$c
        break;
    fi
done

case "$COMMAND" in
    '')
        print_log W "Usage: $0 start" "or: $0 <dockker-compose command>" \
            "Type $0 help to display the docker-compose help"
        exit
    ;;
    start)
        _docker_compose up --no-start
        _docker_compose up -d
    ;;
    *)
        _docker_compose "$@"
    ;;
esac
