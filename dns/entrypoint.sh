#!/bin/sh -e

if [ ! -f "/etc/bind/rndc.key" ]; then
    rndc-confgen -c /etc/bind/rndc.key -b 512 -a -u named
fi

mkdir -p /etc/bind /run/named
chown -R root:named /etc/bind /run/named
chmod -R u=rwX,g=rX,o= /etc/bind

mkdir -p /var/bind
touch /var/bind/named.cache
chown -R named:named /var/bind
chmod -R ug=rwX,o= /var/bind /run/named

exec /usr/sbin/named -c /etc/bind/named.conf -g -u named
