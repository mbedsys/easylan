#!/bin/bash -e

EASYLAN_PKI_DIR=/opt/easylan/data/pki
EASYLAN_SSL_DIR=/opt/easylan/ssl
EASYLAN_CAS_DIR=/opt/easylan/ca-certificates
EASYLAN_NGINX_CONF_D=/opt/easylan/nginx-conf-d
EASYLAN_SHARE_NGINX=/opt/easylan/share/nginx
EASYLAN_DNS_CONFIG=/opt/easylan/dns-config
EASYLAN_DNS_DATA=/opt/easylan/dns-data
EASYLAN_SELF_DOCKER_ID="$(basename "$(cat /proc/1/cpuset)")"
EASYLAN_DC_PRJ_NAME="$(docker inspect "$EASYLAN_SELF_DOCKER_ID" -f '{{index .Config.Labels "com.docker.compose.project"}}')"
declare -A EASYLAN_DOCKER_NAME

: ${EASYLAN_TLD:=lan}
: ${EASYLAN_KEYGEN_OPTS:="--use-algo=ec --curve=secp521r1 --digest=sha512"}
: ${EASYLAN_EXTERNAL_IP4:="127.0.0.1"}
: ${EASYLAN_EXTERNAL_IP6:="::1"}

# display a message
print_log() {
    local level=$1;
    local fmt="%s"
    shift || print_log C "Usage print_log <level> [-f <fmt>] message"
    if [ "$1" == "-f" ]; then
        fmt=$2
        shift 2 || true
    fi
    case "${level,,}" in
        c|critical)
            >&2 printf "\033[91m[CRIT] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            exit 1
            ;;
        e|error)
            >&2 printf "\033[91m[ERRO] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        w|warning)
            >&2 printf "\033[93m[WARN] ${FUNCNAME[1]}: $fmt\033[0m\n" "$@"
            ;;
        n|note)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$@"
            ;;
        i|info)
            printf "\033[92m[INFO] $fmt\033[0m\n" "$@"
            ;;
        *)
            printf "[NOTE] ${FUNCNAME[1]}: $fmt\n" "$level" "$@"
            ;;
    esac
}

easyrsa() {
    local easyrsa_opts=(
        --batch
        $EASYLAN_KEYGEN_OPTS
        --pki-dir="$EASYLAN_PKI_DIR"
        --days=${EASYLAN_CRT_DAYS:-365}
        --req-c="$EASYLAN_REQ_COUNTRY"
        --req-st="$EASYLAN_REQ_STATE"
        --req-city="$EASYLAN_REQ_CITY"
        --req-org="$EASYLAN_REQ_ORG"
        --req-email="$EASYLAN_REQ_EMAIL"
        --req-ou="$EASYLAN_REQ_OU"
    )
    /usr/share/easy-rsa/easyrsa "${easyrsa_opts[@]}" "$@"
}

easylan_dns_service_update() {
    local container=$1 domain_name service_name dns_alt_names default_cn name
    eval "$(docker inspect $container --format '
            host_name="{{.Config.Hostname}}"
            domain_name="{{.Config.Domainname}}"
            service_name="{{index .Config.Labels "com.docker.compose.service"}}"
            service_ip4="{{.NetworkSettings.Networks.'"$EASYLAN_DC_PRJ_NAME"'_backend.IPAddress}}"
            service_ip6="{{.NetworkSettings.Networks.'"$EASYLAN_DC_PRJ_NAME"'_backend.GlobalIPv6Address}}"
            config="$(echo "{{index .Config.Labels "lan.easylan.dns.config"}}")"
        ')"
    if [ -z "$host_name" ] || [ "$host_name" == "${container:0:${#host_name}}" ]; then
        host_name="$service_name"
    else
        host_name="${host_name%.$EASYLAN_TLD}"
    fi
    default_cn="${domain_name:-"$host_name.$EASYLAN_TLD"}"
    test -n "$config" || config="dummy=default"
    read -a config <<< "$config"
    for conf_elt in "${config[@]}"; do
        unset props
        # default values
        declare -A props=(
            [name]="$default_cn"
            [ip6-int]="${service_ip6:-none}"
            [ip4-int]="${service_ip4:-none}"
            [ip6-ext]="${EASYLAN_EXTERNAL_IP6:-none}"
            [ip4-ext]="${EASYLAN_EXTERNAL_IP4:-none}"
        )
        # Fill value from the config
        eval "$(tr ';' '\n' <<< "$conf_elt" | sed -E 's/^([^=]+)/props[\1]/g')"
        if [ -z "${props[name]}" ]; then
            print_log W "Empty CN for the service $service_name, skipping this entry..."
            continue
        fi
        if [ "${props[ip6-int]}" != "none" ]; then
            name=${props[name]%.$EASYLAN_TLD}
            print_log N "Add AAAA DNS record $name to ${props[ip6-int]} into the internal zone"
            dns_lanzone_int+=("$name	IN	AAAA	${props[ip6-int]}")
        fi
        if [ "${props[ip4-int]}" != "none" ]; then
            name=${props[name]%.$EASYLAN_TLD}
            print_log N "Add A DNS record $name to ${props[ip4-int]} into the internal zone"
            dns_lanzone_int+=("$name	IN	A	${props[ip4-int]}")
        fi
        if [ "${props[ip6-ext]}" != "none" ]; then
            name=${props[name]%.$EASYLAN_TLD}
            print_log N "Add AAAA DNS record $name to ${props[ip6-ext]} into the external zone"
            dns_lanzone_ext+=("$name	IN	AAAA	${props[ip6-ext]}")
        fi
        if [ "${props[ip4-ext]}" != "none" ]; then
            name=${props[name]%.$EASYLAN_TLD}
            print_log N "Add A DNS record $name to ${props[ip4-ext]} into the external zone"
            dns_lanzone_ext+=("$name	IN	A	${props[ip4-ext]}")
        fi
    done
}

easylan_dns_global_config_update() {
    local container dns_lanzone_int=() dns_lanzone_ext=()
    easylan_check_service dns || return 0
    printf "%s\n" 'view "internal" {'                                   \
        '    match-clients { ::1/128; 127.0.0.0/8; 172.22.0.0/24; };'   \
        ''                                                              \
        '    include "/etc/bind/named.conf.default-zones";'             \
        ''                                                              \
        '    zone "'"$EASYLAN_TLD"'" IN {'                              \
        '            type master;'                                      \
        '            file "pri/'"$EASYLAN_TLD"'.int.db";'               \
        '            notify no;'                                        \
        '    };'                                                        \
        '};'                                                            \
        ''                                                              \
        'view "external" {'                                             \
        '    match-clients { any; };'                                   \
        ''                                                              \
        '    include "/etc/bind/named.conf.default-zones";'             \
        ''                                                              \
        '    zone "'"$EASYLAN_TLD"'" IN {'                              \
        '            type master;'                                      \
        '            file "pri/'"$EASYLAN_TLD"'.ext.db";'               \
        '            notify no;'                                        \
        '    };'                                                        \
        '};' > "$EASYLAN_DNS_CONFIG/named.conf.views"
    printf "%s\n" 'dnssec-enable yes;' 'dnssec-validation auto;'        \
        'dnssec-must-be-secure '"$EASYLAN_TLD"' no;' > "$EASYLAN_DNS_CONFIG/named.conf.dnssec"
    local date=$(date +%Y%m%d)
    local zone_file_int="$EASYLAN_DNS_DATA/pri/$EASYLAN_TLD.int.db"
    local zone_file_ext="$EASYLAN_DNS_DATA/pri/$EASYLAN_TLD.ext.db"
    local serial_cur=$(test ! -f "$zone_file_ext" \
        || awk '/^\s+[0-9]+\s*; Serial$/ {print $1}' "$zone_file_ext")
    local serial_base=${serial_cur:0:8}
    local serial_index
    if [ "$serial_base" == "$date" ]; then
        serial_index=$(($(sed -E 's/^.{8}0*//g' <<< $serial_cur) + 1))
    else
        serial_base=$date
        serial_index=0
    fi
    dns_lanzone_int=(
        "\$ORIGIN	$EASYLAN_TLD."
        "\$TTL	1H"
        "@	IN	SOA	ns0.$EASYLAN_TLD.	hostmaster.$EASYLAN_TLD. ("
        "						$(printf "%08d%02d" "$serial_base" "$serial_index")	; Serial"
        "						8H		; Refresh"
        "						2H		; Retry"
        "						1W		; Expire - 1 week"
        "						1H)		; Minimum"
        ""
        "@		IN	NS		ns0"
    )
    dns_lanzone_ext=("${dns_lanzone_int[@]}")
    for container in $(docker ps --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
        --filter 'label=lan.easylan.dns.enable=True' -q); do
        easylan_dns_service_update $container
    done
    printf "%s\n" "${dns_lanzone_int[@]}" > "$zone_file_int.staging"
    printf "%s\n" "${dns_lanzone_ext[@]}" > "$zone_file_ext.staging"
    if docker exec "${EASYLAN_DOCKER_NAME[dns]}" named-checkzone "$EASYLAN_TLD" \
        "/var/bind/pri/$EASYLAN_TLD.int.db.staging" > /dev/null \
        && docker exec "${EASYLAN_DOCKER_NAME[dns]}" named-checkzone "$EASYLAN_TLD" \
        "/var/bind/pri/$EASYLAN_TLD.ext.db.staging" > /dev/null; then
        mv "$zone_file_int.staging" "$zone_file_int"
        mv "$zone_file_ext.staging" "$zone_file_ext"
        chown 100:101 "$zone_file_int" "$zone_file_ext"
        docker exec "${EASYLAN_DOCKER_NAME[dns]}" rndc -k /etc/bind/rndc.key reload > /dev/null
        print_log I "DNS configuration successfully updated"
        return 0
    else
        return 1
    fi
}

easylan_certbot_certonly() {
    local certbot_args=(certonly --webroot --agree-tos --non-interactive --preferred-challenges http-01)
    if [ "$EASYLAN_CERTBOT_TEST" == "true" ]; then
        certbot_args+=(--test-cert)
    fi
    if [ -z "$EASYLAN_CERTBOT_EMAIL" ]; then
        print_log C "Empty variable EASYLAN_CERTBOT_EMAIL!" \
            "You must fill this variable with your email address into the env.d/easylan.env file" \
            "and restart this service"
    fi
    certbot_args+=(--email "$EASYLAN_CERTBOT_EMAIL" --webroot-path /var/www/html)
    certbot "${certbot_args[@]}" "$@"
}

easylan_ssl_service_update() {
    local container=$1 domain_name service_name dns_alt_names default_cn restart=0 renew
    eval "$(docker inspect $container --format '
            host_name="{{.Config.Hostname}}"
            domain_name="{{.Config.Domainname}}"
            service_name="{{index .Config.Labels "com.docker.compose.service"}}"
            config="$(echo "{{index .Config.Labels "lan.easylan.ssl.config"}}")"
        ')"
    if [ -z "$host_name" ] || [ "$host_name" == "${container:0:${#host_name}}" ]; then
        host_name="$service_name"
    else
        host_name="${host_name%.$EASYLAN_TLD}"
    fi
    test -n "$config" || config="dummy=default"
    read -a config <<< "$config"
    default_cn="${domain_name:-"$host_name.$EASYLAN_TLD"}"
    for conf_elt in "${config[@]}"; do
        unset props
        renew=0
        # default values
        declare -A props=(
            [cn]="$default_cn"
            [ca]="ca.crt"
            [key-uid]=0 [key-gid]=0 [key-mod]=0600
            [crt-uid]=0 [crt-gid]=0 [crt-mod]=0644
            [ca-uid]=0 [ca-gid]=0 [ca-mod]=0644
            [dhparam-size]=4096
            [dhparam-uid]=0 [dhparam-gid]=0 [dhparam-mod]=0600
        )
        # Fill value from the config
        eval "$(tr ';' '\n' <<< "$conf_elt" | sed -E 's/^([^=]+)/props[\1]/g')"
        test -n "${props[key]}" || props[key]="${props[cn]}.key"
        test -n "${props[crt]}" || props[crt]="${props[cn]}.crt"
        # setup DNS alt names
        read -a dns_alt_names <<< "${props[dns-alt-names]//,/ }"
        dns_alt_names="DNS:${props[cn]}$(for dn in "${dns_alt_names[@]}"; do
            echo -n ",DNS:${dn%.$EASYLAN_TLD}.$EASYLAN_TLD"
        done)"
        # Case: cert exists and (expired cert or dns_alt_names changed)
        if [ -f "$EASYLAN_PKI_DIR/issued/${props[cn]}.crt" ]; then
            if ! openssl x509 -noout -in "$EASYLAN_PKI_DIR/issued/${props[cn]}.crt" -checkend $((2592000)) > /dev/null; then
                print_log N "The certificate will expire within the next 30 days... Certificate renew needed"
                renew=1
            elif [ -n "$(comm -13 <(openssl x509 -noout -in "$EASYLAN_PKI_DIR/issued/${props[cn]}.crt" -text -certopt \
                    no_subject,no_header,no_version,no_serial,no_signame,no_validity,no_issuer,no_pubkey,no_sigdump,no_aux \
                | grep -E '^\s*DNS:' | sed -E 's/\s+DNS:([^,]+),?/\1\n/g' | sed -E '/^$/d' | sort -u) \
                <(tr ',' '\n' <<< "${props[cn]},${props[dns-alt-names]}" | sort -u))" ]; then
                print_log N "Missing some domain names in the certificate... Certificate renew needed"
                renew=1
            fi
            if [ $renew -eq 1 ]; then
                if [ "$EASYLAN_CERT_TYPE" == "easyrsa" ]; then
                    print_log N "Revoking the certificate ${props[cn]} for the service $service_name..."
                    easyrsa revoke "${props[cn]}"
                    easyrsa gen-crl
                fi
                rm -f "$EASYLAN_PKI_DIR/reqs/${props[cn]}.req"
            fi
        else
            rm -f "$EASYLAN_PKI_DIR/reqs/${props[cn]}.req"
        fi
        if [ ! -f "$EASYLAN_PKI_DIR/reqs/${props[cn]}.req" ]; then
            print_log N "DNS alt names: $dns_alt_names"
            easyrsa --req-cn="${props[cn]}" --subject-alt-name="$dns_alt_names" gen-req "${props[cn]}" nopass
            case "$EASYLAN_CERT_TYPE" in
                letsencrypt)
                    easylan_proxy_sanity_check
                    rm -f "$EASYLAN_PKI_DIR/letsencrypt-chain.pem"
                    rm -f /var/log/letsencrypt/letsencrypt.log*
                    if ! easylan_certbot_certonly --csr "$EASYLAN_PKI_DIR/reqs/${props[cn]}.req" \
                        --cert-path "$EASYLAN_PKI_DIR/issued/${props[cn]}.crt" \
                        --chain-path "$EASYLAN_PKI_DIR/letsencrypt-chain.pem" -d "${props[cn]}" \
                        $(test -z "${props[dns-alt-names]}" || echo "-d ${props[dns-alt-names]}"); then
                        return
                    fi
                    mv "$EASYLAN_PKI_DIR/letsencrypt-chain.pem" "$EASYLAN_PKI_DIR/chain.pem"
                ;;
                easyrsa)
                    easyrsa --req-cn="${props[cn]}" --subject-alt-name="$dns_alt_names" sign-req server "${props[cn]}"
                ;;
                *)
                    print_log C "Invalid or unsupported certificate type (EASYLAN_CERT_TYPE=$EASYLAN_CERT_TYPE)"
                ;;
            esac
            rm -f "$EASYLAN_SSL_DIR/$service_name/"{${props[crt]},${props[key]},${props[ca]}}
            print_log I "Certificate ${props[cn]} generated for the service $service_name"
        fi
        mkdir -p "$EASYLAN_SSL_DIR/$service_name"
        if [ ! -f "$EASYLAN_SSL_DIR/$service_name/${props[crt]}" \
            -o ! -f "$EASYLAN_SSL_DIR/$service_name/${props[key]}" \
            -o ! -f "$EASYLAN_SSL_DIR/$service_name/${props[ca]}" ]; then
            print_log N "Installing key file into $EASYLAN_SSL_DIR/$service_name/${props[key]}..."
            cp "$EASYLAN_PKI_DIR/private/${props[cn]}.key" "$EASYLAN_SSL_DIR/$service_name/${props[key]}"
            chown ${props[key-uid]}:${props[key-gid]} "$EASYLAN_SSL_DIR/$service_name/${props[key]}"
            chmod ${props[key-mod]} "$EASYLAN_SSL_DIR/$service_name/${props[key]}"
            print_log N "Installing certificate chain into $EASYLAN_SSL_DIR/$service_name/${props[crt]}..."
            cat "$EASYLAN_PKI_DIR/issued/${props[cn]}.crt" "$EASYLAN_PKI_DIR/chain.pem" >> "$EASYLAN_SSL_DIR/$service_name/${props[crt]}"
            if [ "${props[crt]}" != "${props[key]}" ]; then
                chown ${props[crt-uid]}:${props[crt-gid]} "$EASYLAN_SSL_DIR/$service_name/${props[crt]}"
                chmod ${props[crt-mod]} "$EASYLAN_SSL_DIR/$service_name/${props[crt]}"
            fi
            print_log N "Installing CA file into $EASYLAN_SSL_DIR/$service_name/${props[ca]}..."
            cp "$EASYLAN_PKI_DIR/chain.pem" "$EASYLAN_SSL_DIR/$service_name/${props[ca]}"
            chown ${props[ca-uid]}:${props[ca-gid]} "$EASYLAN_SSL_DIR/$service_name/${props[ca]}"
            chmod ${props[ca-mod]} "$EASYLAN_SSL_DIR/$service_name/${props[ca]}"
            restart=1
        fi
        if [ -n "${props[dhparam]}" ] && [ ! -f "$EASYLAN_SSL_DIR/$service_name/${props[dhparam]}" ]; then
            print_log N "Generating DH parameters file file into $EASYLAN_SSL_DIR/$service_name/${props[dhparam]}..."
            openssl dhparam -out "$EASYLAN_SSL_DIR/$service_name/${props[dhparam]}" "${props[dhparam-size]}"
            chown ${props[dhparam-uid]}:${props[dhparam-gid]} "$EASYLAN_SSL_DIR/$service_name/${props[dhparam]}"
            chmod ${props[dhparam-mod]} "$EASYLAN_SSL_DIR/$service_name/${props[dhparam]}"
            restart=1
        fi
    done
    if [ $restart -eq 1 ]; then
        if [ "$service_name" == "proxy" ]; then
            easylan_proxy_global_config_update
        else
            print_log N "Restarting the service $service_name..."
            docker restart $container > /dev/null
        fi
    fi
}

easylan_proxy_sanity_check() {
    if [ -e "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.crt" ] \
        && [ ! -e "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.key" ] \
        && [ ! -e "$EASYLAN_SSL_DIR/proxy/ca.crt" ] \
        && [ "$(openssl x509 -noout -modulus -in $EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.crt | sha512sum)" \
            == "$(openssl rsa -noout -modulus -in $EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.key | sha512sum)" ]; then
        return 0
    fi
    print_log W "Restoring the default proxy key and certificate..."
    mkdir -p "$EASYLAN_SSL_DIR/proxy"
    cp "$EASYLAN_SHARE_NGINX/proxy_init.crt" "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.crt"
    cp "$EASYLAN_SHARE_NGINX/proxy_init.key" "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.key"
    cp "$EASYLAN_SHARE_NGINX/ca_init.crt" "$EASYLAN_SSL_DIR/proxy/ca.crt"
    openssl dhparam -out "$EASYLAN_SSL_DIR/proxy/dhparam.pem" 1024
    chown 0:0 "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.crt" \
        "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.key" "$EASYLAN_SSL_DIR/proxy/ca.crt"
    chmod 0600 "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.key"
    chmod 0644 "$EASYLAN_SSL_DIR/proxy/proxy.$EASYLAN_TLD.crt" "$EASYLAN_SSL_DIR/proxy/ca.crt"
    print_log W "Restarting proxy..."
    docker restart "${EASYLAN_DOCKER_NAME[proxy]}"
    print_log W "Wait until the proxy service is fully started..."
    sleep 10
    until curl http://proxy.$EASYLAN_TLD/ -o /dev/null > /dev/null 2>&1; do
        sleep 10
        nslookup proxy.$EASYLAN_TLD > /dev/null 2>&1 || easylan_dns_global_config_update
    done
    rm -f "$EASYLAN_SSL_DIR/proxy/*"
}

easylan_ssl_global_update() {
    local container
    if [ ! -d "$EASYLAN_PKI_DIR" ]; then
        print_log N "Initializing PKI..."
        rm -rf "$EASYLAN_PKI_DIR"
        easyrsa init-pki
    fi
    if [ ! -f "$EASYLAN_PKI_DIR/ca.crt" ]; then
        print_log N "Generating CA certificate..."
        EASYLAN_CRT_DAYS=10950 easyrsa --req-cn="${EASYLAN_CA_CN:-"EasyLAN CA"}" build-ca nopass
        cat "$EASYLAN_PKI_DIR/ca.crt" > "$EASYLAN_CAS_DIR/easylan-ca.crt"
        cat "$EASYLAN_PKI_DIR/ca.crt" > "$EASYLAN_SSL_DIR/easylan-ca.crt"
        chmod a+r "$EASYLAN_CAS_DIR/easylan-ca.crt" "$EASYLAN_SSL_DIR/easylan-ca.crt"
        docker restart $(docker ps --no-trunc --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
            -a -q | grep -v ^$EASYLAN_SELF_DOCKER_ID$) 2> /dev/null || true
    fi
    case "$EASYLAN_CERT_TYPE" in
        letsencrypt)
            print_log I "Using LetsEncrypt certificate type"
            if [ -z "$EASYLAN_CERTBOT_EMAIL" ]; then
                print_log C "An email address is needed (empty EASYLAN_CERTBOT_EMAIL variable)"
            fi
            if [ ! -d "/etc/letsencrypt/accounts" ]; then
                certbot --non-interactive --agree-tos register --email "$EASYLAN_CERTBOT_EMAIL"
            fi
        ;;
        easyrsa)
            print_log I "Using self signed (using EasyRSA) certificate type"
            cp -a "$EASYLAN_PKI_DIR/ca.crt" "$EASYLAN_PKI_DIR/chain.pem"
        ;;
        *)
            print_log C "Invalid or unsupported certificate type (EASYLAN_CERT_TYPE=$EASYLAN_CERT_TYPE)"
        ;;
    esac
    for container in $(docker ps -a --no-trunc --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
        --filter 'label=lan.easylan.ssl.enable=True' -q -a); do
        easylan_ssl_service_update $container
    done
    LAST_GLOBAL_CERT_UPDATE=$(date '+%s')
}

easylan_process_template() {
    env "$@" awk '{
            while(match($0,"[$]{[^}]*}")) {
                var=substr($0,RSTART+2,RLENGTH -3);
                gsub("[$]{"var"}",ENVIRON[var])
            }
        }1'
}

easylan_proxy_service_config_update() {
    easylan_check_service proxy || return 0
    local container=$1 host_name domain_name service_name ip_address config conf_elt
    eval "$(docker inspect $container --format '
            host_name="{{.Config.Hostname}}"
            domain_name="{{.Config.Domainname}}"
            service_name="{{index .Config.Labels "com.docker.compose.service"}}"
            ip_address="{{.NetworkSettings.Networks.'"$EASYLAN_DC_PRJ_NAME"'_backend.IPAddress}}"
            config="{{index .Config.Labels "lan.easylan.proxy.config"}}"
        ')"
    if [ -z "$host_name" ] || [ "$host_name" == "$container" ]; then
        host_name="$service_name"
    else
        host_name="${host_name%.$EASYLAN_TLD}"
    fi
    test -n "$config" || config="dummy=default"
    read -a config <<< "$config"
    for conf_elt in "${config[@]}"; do
        unset props
        declare -A props=(
            [server]="${domain_name:-"$host_name.$EASYLAN_TLD"}"
            [proto]="http"
            [address]="$ip_address"
            [port]="80"
            [template]="generic"
        )
        eval "$(tr ';' '\n' <<< "$conf_elt" | sed -E 's/^([^=]+)/props[\1]/g')"
        if [ -z "${props[server]}" ]; then
            print_log W "DNS - Service $service_name, config element '$conf_elt': empty server name"
            continue
        fi
        EASYLAN_PROXY_SERVER_LIST+=(${props[server]})
        if [ -z "${props[address]}" ]; then
            print_log W "Empty address for the service $service_name and server ${props[server]}, skipping this element..."
            continue
        fi
        print_log N "Add proxy_pass from https://${props[server]} to ${props[proto]}://${props[address]}:${props[port]}"
        cat "$EASYLAN_SHARE_NGINX/proxy-${props[template]}.conf.tmpl" \
            | easylan_process_template SERVERS="${props[server]}" NAME="$service_name-${props[server]}" TLD="$EASYLAN_TLD" \
                PROXY_PASS="${props[proto]}://${props[address]}:${props[port]}" > "$EASYLAN_NGINX_CONF_D/$service_name-${props[server]}.conf"
        print_log I "Nginx configuration generated for the service $service_name and server ${props[server]}"
    done
}

easylan_proxy_config_apply() {
    print_log N "Restarting the service proxy..."
    if [ "$(docker inspect "${EASYLAN_DOCKER_NAME[proxy]}" -f "{{.State.Running}}")" == "true" ] \
        && [ "$1" != "restart" ]; then
        docker exec "${EASYLAN_DOCKER_NAME[proxy]}" nginx -s reload \
            || docker restart "${EASYLAN_DOCKER_NAME[proxy]}" > /dev/null
    else
        docker restart "${EASYLAN_DOCKER_NAME[proxy]}" > /dev/null
    fi
    EASYLAN_PROXY_SERVER_LIST=($(printf "%s\n" "${EASYLAN_PROXY_SERVER_LIST[@]}" | sort -u))
    EASYLAN_PROXY_SERVERS="$(printf "%s\n" "${EASYLAN_PROXY_SERVER_LIST[@]}" | tr '\n' ',' | sed 's/,$//g')"
    EASYLAN_PROXY_DNS_CONFIG="$(printf "name=%s " "${EASYLAN_PROXY_SERVER_LIST[@]}")"
}

easylan_proxy_global_config_update() {
    local container
    easylan_check_service proxy || return 0
    rm -f $EASYLAN_NGINX_CONF_D/*
    cat "$EASYLAN_SHARE_NGINX/default.conf.tmpl" \
        | easylan_process_template TLD="$EASYLAN_TLD" > "$EASYLAN_NGINX_CONF_D/default.conf"
    for container in $(docker ps -a --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
        --filter 'label=lan.easylan.proxy.enable=True' -q); do
        easylan_proxy_service_config_update $container
    done
    easylan_proxy_config_apply restart
}

easylan_on_container_start() {
    local var container service proxy ssl
    eval "$@"
    if [ "$proxy" == "True" ]; then
        easylan_proxy_service_config_update $container
        easylan_proxy_config_apply
        easylan_ssl_global_update
    fi
    if [ "$ssl" == "True" ]; then
        easylan_ssl_service_update $container
    fi
}

easylan_service2name() {
    docker ps --no-trunc --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
        --filter 'label=com.docker.compose.service='"$1" -a --format '{{.Names}}'
}

easylan_check_service() {
    local service_name=$1
    if [ -n "${EASYLAN_DOCKER_NAME[$service_name]}" ]; then
        return 0
    fi
    print_log N "Checking service $service_name..."
    EASYLAN_DOCKER_NAME[$service_name]="$(easylan_service2name "$service_name")"
    test -n "${EASYLAN_DOCKER_NAME[$service_name]}" || return 1
}

easylan_main() {
    local container pid1 pid2 i=10
    print_log N "Starting Intranet Manager..."
    while (( i-- > 0 )); do
        easylan_check_service proxy && easylan_check_service dns && break
        sleep 1
    done
    easylan_dns_global_config_update || true
    easylan_proxy_global_config_update
    easylan_ssl_global_update

    # start a background pipeline with two processes running forever
    tail -f /dev/null | tail -f /dev/null &
    # save the process ids
    pid2=$!
    pid1=$(jobs -p %+)
    # hijack the pipe's file descriptors using procfs
    exec 3>/proc/$pid1/fd/1 4</proc/$pid2/fd/0
    # kill the background processes we no longer need
    # (using disown suppresses the 'Terminated' message)
    disown $pid2
    kill $pid1 $pid2

    # anything we write to fd 3 can be read back from fd 4
    docker events --filter event=start  --filter 'type=container' \
        --filter 'label=com.docker.compose.project='"$EASYLAN_DC_PRJ_NAME" \
        --format '{{.ID}}' >&3 &

    easylan_dns_global_config_update || true
    easylan_proxy_global_config_update
    print_log I "Intranet manager service started"
    while true; do
        if read -t 60 -r container <&4; then
            eval "$(docker inspect $container --format \
                'easylan_on_container_start container="{{.ID}}" \
                    service="{{index .Config.Labels "com.docker.compose.service"}}" \
                    proxy="{{index .Config.Labels "lan.easylan.proxy.enable"}}" \
                    ssl="{{index .Config.Labels "lan.easylan.ssl.enable"}}"')"
            easylan_dns_global_config_update
        fi
        if [ $(date '+%s') -gt $(($LAST_GLOBAL_CERT_UPDATE + 86400)) ]; then
            print_log N "Performing the daily certificate update..."
            easylan_ssl_global_update
        fi
    done
    # close the file descriptors when we are finished (optional)
    exec 3>&- 4<&-
}

easylan_main
