# EasyLAN

Overall, this solution aims to deploy a containerized application quickly without having to manage various recurring or even daunting aspects, such as the implementation of an HTTP proxy, an SSL certificate or a DNS server.

List of services provided:
* Automated generation of SSL certificates with a PKI and a self-signed and local CA or from Let's Encrypt
* Automated generation of an HTTPS proxy for all services
* Automated generation of a DNS server with a DNS zone for local services

This solution is mainly designed for the following cases:
* Deployment of web application in a private network for one of the following reasons:
  * you do not have the possibility to obtain a valid certificate provided by a recognized CA
  * you do not have the possibility to get a public domain name
* For a test or development environment, to test an application without having to manage the proxy/SSL/DNS part

## Index
* [EasyLAN project presentation](#easylan-project-presentation)
* [List of main services](#list-of-main-services)
  * [EasyLAN Service](#easylan-service)
    * [Service configuration](#service-configuration)
    * [Certificate settings for each service](#certificate-settings-for-each-service)
  * [DNS service](#dns-service)
    * [Service description](#service-description)
    * [Setting the DNS functionality of services](#setting-the-dns-functionality-of-services)
  * [HTTPS Proxy Service](#https-proxy-service)
    * [Service description](#service-description)
      * [Setting the DNS functionality of services](#setting-the-dns-functionality-of-services)
  * [Label format such as `lan.easylan.*****.config`](#label-format-such-as-laneasylanconfig)

## EasyLAN project presentation

![Project organization](./doc/img/project_architecture.png)

The project includes on the one hand the essential services ("core services" in the diagram) and on the other hand the user services/applications ("guest services" in the diagram).

The project tree is composed among others:
* the `docker-compose.yml` file which contains the description of the essential services
* the `docker-compose.d folder which contains the description of the user services in a file `service_name.yml` overwritten by the file `service_name.override.yml`.
* the `env.d folder which contains definitions of service environments, where `service_name.default.env` is used as a reference to the `service_name.env` file
* the file `config.default.env` used as a reference to the file `config.env` which contains the global parameters
* the `easylan.sh` file that overrides the `docker-compos` command using the `docker-compos.yml` files as well as the `yml` files of the `docker-compose.d folder depending on the services enabled in the `config.env` file
* the data folder (created at first start) containing among other things the generated certificates including the file `data/ssl/easylan-ca.crt` which contains the root certificate

## List of main services

A script at the root `easylan.sh` overloads the command `docker-compose` and allows, based on the configuration `config.env` to launch the different activated services.

Quick Start Guide :
* Run the command `./easylan.sh config` to initialize the configuration
* Edit the configuration `config.env` by updating the variables :
  * EASYLAN_APPLICATION_LIST: application list to activate (ex: EASYLAN_APPLICATION_LIST=(application1 application2 application3))
  * EASYLAN_DNS_LISTEN : DNS service address and listening port. Can be set to 0.0.0.0.0:53 (or 192.168.1.1.10:53 if referring to the diagram)
  * EASYLAN_PROJECT_NAME (advanced setting) : docker-compose project name (advanced use)
  * EASYLAN_NET4_BASE (advanced setting): Network address database of the service subnetwork (advanced use)
  * EASYLAN_DNS_SERVER (advanced setting): DNS server internal IP (advanced use)
* Run the command `./easylan.sh config` again to initialize the configuration of the applications you activated in the previous step
* Go to the configuration of the main services (file `env.d/easylan.env`, see topics below) and applications before proceeding to the next step
* Finally, execute the command `./easylan.sh start`.

### EasyLAN Service

#### Service configuration

List of environment variables (file env.d/easylan.env) :
* EASYLAN_TLD: Domain extension to be used, from which sub-domains of the type my-service.my-extension will result (example `lan` for local use, or example.com, or even, my-services-lan.example.com)
* EASYLAN_CA_CN : Name of the local certification authority
* EASYLAN_EXTERNAL_IP4 : External IP (v4) of the server, e. g. 192.168.1.10 if we refer to the schema (it is not necessarily the public IP address)
* EASYLAN_EXTERNAL_IP6: External IP (v6) of the server (same remarks as for parameter EASYLAN_EXTERNAL_IP4)
* EASYLAN_CERT_TYPE : Certificate type : `easyrsa` (default) for local use with a self-signed certification authority, or `letsencrypt` to generate certificates using the Let's Encrypt service (requires a public domain with a wildcard CNAME DNS field to the server)
* EASYLAN_KEYGEN_OPTS (advanced parameter): Key generation option for the easyrsa command. It is recommended to leave the algorithm to RSA to avoid incompatibility problems related to certain services
* EASYLAN_CERTBOT_TEST (use with EASYLAN_CERT_TYPE=letsencrypt): Let's Encrypt test certificate use
* EASYLAN_CERTBOT_EMAIL (use with EASYLAN_CERT_TYPE=letsencrypt): Valid email required for registration with Let's Encrypt

#### Certificate settings for each service

List of possibilities in basic to advanced order:
* To enable this feature you must add the following label: `lan.easylan.ssl.enable: true`
* To define a custom configuration (which replaces the default configuration) you must add the following label: `lan.easylan.ssl.config: "my config"` using the [following format](#label-format-such-as-laneasylanconfig)
* The default configuration is a configuration item with all the default values (list below)
* List of configuration fields:
  * `cn` (default: `service_name.$EASYLAN_TLD`) : Main domain name of the service
  * `dns-alt-names`: Alternative domain names of the service
  * `key` (default: `service_name.$EASYLAN_TLD.key`) : name of the key file
  * `crt` (default: `service_name.$EASYLAN_TLD.crt`) : Certificate file name
  * `ca` (default: `ca.crt`) : File name of the certification authority
  * `dhparam` (default: none) : Diffie-Hellman file
  * `dhparam-size` (default: `1024`) : Diffie-Hellman key size
  * `key-uid`, `crt-uid`, `ca-uid`, `dhparam-uid` (default: `0`) : User numerical identifier associated with the file
  * `key-gid`, `crt-gid`, `ca-gid`, `dhparam-gid` (default: `0`) : Group numerical identifier associated with the file
  * `key-mod`, `crt-mod`, `ca-mod`, `dhparam-mod` (default: `0700`) : Permissions in digital format associated with the file

Examples: the HTTP proxy service (file [docker-compose.yml](./docker-compose.yml)) with a certificate containing the name `proxy.$EASYLAN_TLD` as well as the non-applications stored in the variable $EASYLAN_PROXY_SERVERS

### DNS service

#### Service description
The DNS service contains the configuration of the `$EASYLAN_TLD` zone with :
* an internal view for the services and application of the backend network that resolves the name to the internal IP if it is queried from this internal network
* an external view will systematically return `$EASYLAN_EXTERNAL_IP4` or `$EASYLAN_EXTERNAL_IP6` (192.168.1.10 on the diagram) for services accessible from outside (e.g. HTTPS proxy, DNS server or MySQL server on the example)

#### Setting the DNS functionality of services

List of possibilities in basic to advanced order:
* To enable this feature you must add the following label: `lan.easylan.dns.enable: true`
* To define a custom configuration (which replaces the default configuration) you must add the following label: `lan.easylan.dns.config: "my config"` using the [following format](#label-format-such-as-laneasylanconfig)
* The default configuration is a configuration item with all the default values (list below)
* List of configuration fields:
  * `name` (default: service name or host name if specified): name that will give the DNS name.zone_name (example `mysql.lan` or `mysql.example.com`)
  * `ip6-int` (default: IPv6 of the docker on the private network) : Definition of IPv6 on the internal view
  * `ip4-int` (default: IPv4 of the docker on the private network) : Definition of IPv4 on the internal view
  * `ip6-ext` (default: value of the environment variable EASYLAN_EXTERNAL_IP6) : Definition of IPv6 on the external view
  * `ip4-ext` (default: value of the environment variable EASYLAN_EXTERNAL_IP4) : Definition of IPv4 on the external view

Examples :
* PostgreSQL application (file [docker-compose.d/postgres.yml](docker-compose.d/postgres.yml)) : Simple activation with the default generated name (`postgres.$EASYLAN_TLD`)
* MariaDB application (file [docker-compose.d/mariadb.yml](docker-compose.d/mariadb.yml)) : Activation + definition of two names (`mariadb.$EASYLAN_TLD` and `mysql.$EASYLAN_TLD`)

### HTTPS Proxy Service

#### Service description

The HTTPS Proxy service is a service that can be used by all applications and allows:
* Quick and easy definition of an HTTP proxy
* Secure communication via HTTPS included

##### Setting the proxy functionality of services

List of possibilities in basic to advanced order:
* To enable this feature you must add the following label: `lan.easylan.proxy.enable: true`
* To define a custom configuration (which replaces the default configuration) you must add the following label: `lan.easylan.proxy.config: "my config"` using the [following format](#label-format-such-as-laneasylanconfig)
* The default configuration is a configuration item with all the default values (list below)
* List of configuration fields:
  * `server` (default: service name or host name if specified) : Name of the server (domain name) allowing access to the application via proxy
  * `proto` (default: `http`): protocol of the target application to which the proxy will redirect the flow (http or https)
  * `address` (default: `$ip_address`) : address of the target application to which the proxy will redirect the flow
  * `port` (default: `80`): port of the target application to which the proxy will redirect the flow
  * `template` (default: `generic`) : file template to be used to define the proxy configuration

Examples :
* Nextcloud application (file [docker-compose.d/nextcloud.yml](./docker-compose.d/nextcloud.yml)) : Simple activation with the default generated name, here from the hostname suffixed by the TLD or domain name chosen during configuration (`cloud.$EASYLAN_TLD`), so if we refer to the example scheme, any requests sent to https://cloud.lan will be redirected to http://172.22.0.7:80
* MariaDB application (file [docker-compose.d/miniflux.yml](./docker-compose.d/miniflux.yml)) : Enabling + defining a proxy configuration to a custom port 8080

### Label format such as `lan.easylan.*****.config`
Each `config` field is a text field that contains one or more configurations separated by a space. Each of these configurations is a sequence of `key=value` separated by a comma.
Example of configuration `key1=value1 key2=value2,key3=value3` : There are two configurations: `key1=value1` and `key2=value2,key3=value3`
